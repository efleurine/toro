# toro

Trade stocks on the command line using your Alpaca account

```bash
toro report
📈 Portfolio
  $100029.38

 100039 ┼╮                                 ╭─╮ ╭╮╭╮       
 100033 ┤│                               ╭╮│ ╰─╯╰╯╰────── 
 100026 ┤│                           ╭───╯╰╯              
 100020 ┤│              ╭╮       ╭──╮│                    
 100013 ┤│            ╭─╯│     ╭╮│  ╰╯                    
 100007 ┤╰╮          ╭╯  │   ╭─╯╰╯                        
 100001 ┤ │        ╭╮│   │╭─╮│                            
  99994 ┤ ╰──╮     │╰╯   ╰╯ ╰╯                            
  99988 ┤    │╭╮   │                                      
  99981 ┤    ││╰─╮ │                                      
  99975 ┤    ╰╯  ╰─╯                                      
           1 Day

💼 Stocks
  Stock  Shares   Price  Avg Cost   Equity  Today's Return (%)  Total Return (%)
   AAPL       9  306.65    309.74  2759.85               -0.34             -1.00
```

## Usage

- `toro report`
- `toro get SYMBOL`
- `toro buy SYMBOL`
- `toro sell SYMBOL`
- `toro cancel ORDER_ID`
- `toro config [set-credentials set-mode]`

## Setup

### Install toro

```bash
git clone https://gitlab.com/aenegri/toro.git
cd toro
go install
```

### Signup for Alpaca

Create your brokerage account with [alpaca](https://app.alpaca.markets/signup)

### Configure toro

First, provide credentials for toro to access your Alpaca account

```bash
toro config set-credentials --id={id} --secret={secret}
```

Then, decide whether to trade with your paper or live account

```bash
toro config set-mode {paper|live}
```