module gitlab.com/aenegri/toro

go 1.13

require (
	github.com/99designs/keyring v1.1.5
	github.com/alpacahq/alpaca-trade-api-go v1.5.0
	github.com/danieljoos/wincred v1.1.0 // indirect
	github.com/guptarohit/asciigraph v0.4.1
	github.com/shopspring/decimal v1.2.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.0 // indirect
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
	golang.org/x/sys v0.0.0-20200515095857-1151b9dac4a9 // indirect
)
