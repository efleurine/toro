package cmd

import (
	"fmt"
	"io"
	"os"
	"text/tabwriter"

	"github.com/99designs/keyring"
	"github.com/alpacahq/alpaca-trade-api-go/alpaca"
	"github.com/alpacahq/alpaca-trade-api-go/common"
	"github.com/spf13/cobra"
)

var cfgFile string

func getAlpacaClient() (*alpaca.Client, error) {
	ring, err := keyring.Open(keyring.Config{
		ServiceName: "toro",
	})

	if err != nil {
		return nil, err
	}

	id, err := ring.Get("id")

	if err != nil {
		return nil, err
	}

	secret, err := ring.Get("secret")

	if err != nil {
		return nil, err
	}

	mode, err := ring.Get("mode")
	modeText := string(mode.Data)

	if err != nil || modeText == "" {
		fmt.Println("Mode not specified. Defaulting to \"paper\". To trade with real money, set mode to \"live\"")
		modeText = "paper"
	} else if !(modeText == "paper" || modeText == "live") {
		return nil, fmt.Errorf("Mode \"%s\" is invalid", modeText)
	}

	if modeText == "paper" {
		alpaca.SetBaseUrl("https://paper-api.alpaca.markets")
	}

	return alpaca.NewClient(&common.APIKey{
		ID:     string(id.Data),
		Secret: string(secret.Data),
	}), nil
}

func getTabWriter() *tabwriter.Writer {
	return tabwriter.NewWriter(os.Stdout, 0, 4, 2, ' ', tabwriter.AlignRight)
}

type writeFlusher interface {
	io.Writer

	Flush() error
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "toro",
	Short: "toro is a CLI tool for trading stocks",
	Long: `toro is a tool for stock trading with your personal Alpaca account.
	`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	rootCmd.AddCommand(newConfigCmd().GetCommand())
	rootCmd.AddCommand(newGetCmd().GetCommand())
	rootCmd.AddCommand(newBuyCmd().GetCommand())
	rootCmd.AddCommand(newSellCmd().GetCommand())
	rootCmd.AddCommand(newOrdersCmd().GetCommand())
	rootCmd.AddCommand(newCancelCmd().GetCommand())
	rootCmd.AddCommand(newReportCmd().GetCommand())
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
