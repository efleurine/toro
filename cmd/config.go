package cmd

import (
	"errors"
	"fmt"

	"github.com/99designs/keyring"
	"github.com/spf13/cobra"
)

type configCmd struct {
	cmd *cobra.Command
}

func newConfigCmd() *configCmd {
	c := &configCmd{
		cmd: &cobra.Command{
			Use:   "config",
			Short: "Adjust config values",
			Long:  `Adjust config values, including account credentials and other options`,
		},
	}

	c.cmd.AddCommand(newSetCredentialsCmd().GetCommand())
	c.cmd.AddCommand(newSetModeCmd().GetCommand())

	return c
}

func (c *configCmd) GetCommand() *cobra.Command {
	return c.cmd
}

type setCredentialsCmd struct {
	cmd *cobra.Command

	id     string
	secret string
}

func newSetCredentialsCmd() *setCredentialsCmd {
	setCreds := &setCredentialsCmd{
		cmd: &cobra.Command{
			Use:   "set-credentials",
			Short: "Provide credentials for authenticating your Alpaca account",
			Long:  `Provide credentials for authenticating your Alpaca account`,
		},
	}

	setCreds.cmd.RunE = func(cmd *cobra.Command, args []string) error {
		if setCreds.id == "" && setCreds.secret == "" {
			return errors.New("No credentials provided")
		}

		ring, err := keyring.Open(keyring.Config{
			ServiceName: "toro",
		})

		if err != nil {
			return fmt.Errorf("Failed to open keyring: %w", err)
		}

		if setCreds.id != "" {
			err = ring.Set(keyring.Item{
				Key:         "id",
				Data:        []byte(setCreds.id),
				Description: "id for Alapca",
			})

			if err == nil {
				fmt.Printf("✅ Set id successfully\n")
			} else {
				return fmt.Errorf("Failed to save id to keyring: %w", err)
			}
		}

		if setCreds.secret != "" {
			err = ring.Set(keyring.Item{
				Key:         "secret",
				Data:        []byte(setCreds.secret),
				Description: "secret for toro",
			})

			if err == nil {
				fmt.Printf("✅ Set secret successfully\n")
			} else {
				return fmt.Errorf("Failed to save secret to keyring: %w", err)
			}
		}

		return nil
	}

	setCreds.cmd.Flags().StringVar(&setCreds.id, "id", "", "id for api access")
	setCreds.cmd.Flags().StringVar(&setCreds.secret, "secret", "", "secret for api access")

	return setCreds
}

func (setCreds *setCredentialsCmd) GetCommand() *cobra.Command {
	return setCreds.cmd
}

type setModeCmd struct {
	cmd *cobra.Command
}

func newSetModeCmd() *setModeCmd {
	return &setModeCmd{
		cmd: &cobra.Command{
			Use:   "set-mode (live|paper)",
			Short: "Select whether trade will be made using your \"live\" or \"paper\" account",
			RunE: func(cmd *cobra.Command, args []string) error {
				if len(args) < 1 {
					return errors.New("Mode not provided")
				}

				mode := args[0]

				if !(mode == "paper" || mode == "live") {
					return fmt.Errorf("Mode \"%s\" invalid. Mode must be \"live\" or \"paper\"", mode)
				}

				ring, err := keyring.Open(keyring.Config{
					ServiceName: "toro",
				})

				if err != nil {
					return err
				}

				err = ring.Set(keyring.Item{
					Key:         "mode",
					Data:        []byte(mode),
					Description: "mode for toro",
				})

				if err != nil {
					return err
				}

				fmt.Printf("✅ Mode set to \"%s\"\n", mode)

				return nil
			},
		},
	}
}

func (setMode *setModeCmd) GetCommand() *cobra.Command {
	return setMode.cmd
}
